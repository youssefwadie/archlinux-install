#!/bin/sh
ln -sf /usr/share/zoneinfo/Africa/Cairo /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "arch" >> /etc/hostname

echo "127.0.0.1	localhost" >> /etc/hosts
echo "::1		localhost" >> /etc/hosts
echo "127.0.1.1	arch.localdomain	arch" >> /etc/hosts

printf "Installing development packages and network managment system."
pacman -Syu base-devel linux-headers xdg-user-dirs xdg-utils alsa-utils pulseaudio bash-completion doas networkmanager dialog iwd mtools dosfstools ntfs-3g git reflector

echo "permit persist :wheel" >> /etc/doas.conf

# pacman -Syu grub efibootmgr
# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
# grub-mkconfig -o /boot/grub/grub.cfg
systemctl enable NetworkManager
systemctl enable firewalld
printf "Install a bootloader"
printf "Configure passwd, passwd USER and sudoers by visudo."
printf "That's all."
